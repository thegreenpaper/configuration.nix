{
  config,
  pkgs,
  userInfo,
  ...
}: {
  # shell
  programs.zsh.enable = true;
  users.users.${userInfo.username}.shell = pkgs.zsh;

  environment.systemPackages = with pkgs; [
    # terminal emulator
    alacritty
    jetbrains-mono

    # tools
    htop
    curl
    git
    lshw

    # files
    p7zip
    unzip
    unrar
    ventoy

    # nix
    util-linux
    nix-prefetch
    nix-prefetch-git

    # fun
    neovim
    fastfetch
    sl
  ];
}
