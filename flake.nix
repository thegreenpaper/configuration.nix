{
  description = "My main flake";

  inputs = {
    # Package Managers
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    nixpkgs-master.url = "github:NixOS/nixpkgs/master";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # Flake Packages
    stylix.url = "github:danth/stylix";
  };

  outputs = {
    self,
    nixpkgs,
    home-manager,
    stylix,
    nixpkgs-master,
    ...
  } @ inputs: let
    systemInfo = {
      system = "x86_64-linux"; # system arch
      stateVersion = "23.05"; # Don't change this
    };

    userInfo = {
      fullname = "Jørgen Andreas Bratland Bull";
      username = "jorgen";
      # I use Gmail because i am a linux hacker
      email = "jorgenabb@gmail.com";
    };

    # Config
    configurations = {
      theme = "dracula";
      font = {
        name = "Jetbrains Mono";
        pkg = pkgs.jetbrains-mono;
      };
    };

    # OS
    lib = nixpkgs.lib;
    pkgs = import nixpkgs {
      system = systemInfo.system;
      config = {
        allowUnfree = true;
        allowUnfreePredicate = _: true;
      };
    };
    pkgs-master = import nixpkgs-master {
      system = systemInfo.system;
      config = {
        allowUnfree = true;
        allowUnfreePredicate = _: true;
      };
    };
  in {
    homeConfigurations = {
      user = home-manager.lib.homeManagerConfiguration {
        # Note: i did not write this & I am sure this could be done better with flake-utils or something
        inherit pkgs;

        modules = [./home.nix];
        extraSpecialArgs = {
          # pass config variables from above
          inherit systemInfo;
          inherit userInfo;
          inherit configurations;
          inherit inputs;
        };
      };
    };
    nixosConfigurations = {
      homesys = lib.nixosSystem {
        system = systemInfo.system;
        modules = [
          ./desktop.nix
          ./configuration.nix

          ./dwl/dwl.nix

          # entertainment
          ./software/audio/mpd.nix
          ./gaming.nix

          # server
          ./software/server/sshd.nix

          # software
          ./terminal.nix

          # style
          inputs.stylix.nixosModules.stylix
          ./modules/stylix.nix
          ./modules/stylixSys.nix

          # compat
          ./compat/appimage.nix
          ./compat/drawing.nix
          #./compat/vr.nix
          ./compat/nvidia.nix
          ./compat/printing.nix
          ./compat/qmk.nix
          ./compat/android.nix

          # essentials
          ./boot/grub.nix
          ./locale.nix
          ./network.nix
          ./pipewire.nix
          ./compat/flakes.nix
          ./hardware-configuration/brubu.nix

          # cleanup
          ./storage-optimization.nix
        ];
        specialArgs = {
          # pass config variables from above
          inherit systemInfo;
          inherit userInfo;
          inherit configurations;
          inherit pkgs-master;
          inherit inputs;
          hostname = "brubu";
        };
      };
      worksys = lib.nixosSystem {
        system = systemInfo.system;
        modules = [
          ./desktop.nix
          ./configuration.nix

          ./dwl/dwl.nix

          # entertainment
          ./software/audio/mpd.nix
          ./gaming.nix

          # server
          ./software/server/sshd.nix

          # software
          ./terminal.nix

          # style
          inputs.stylix.nixosModules.stylix
          ./modules/stylix.nix
          ./modules/stylixSys.nix

          # compat
          ./compat/appimage.nix
          ./compat/printing.nix
          ./compat/android.nix

          # essentials
          ./boot/systemd-boot.nix
          ./locale.nix
          ./network.nix
          ./pipewire.nix
          ./compat/flakes.nix
          ./hardware-configuration/bruwu.nix

          # cleanup
          ./storage-optimization.nix
        ];
        specialArgs = {
          # pass config variables from above
          inherit systemInfo;
          inherit userInfo;
          inherit configurations;
          inherit pkgs-master;
          inherit inputs;
          hostname = "bruwu"; # yes
        };
      };
      server = lib.nixosSystem {
        system = systemInfo.system;
        modules = [
          ./server/configuration.nix
        ];
        specialArgs = {
          # pass config variables from above
          inherit systemInfo;
          inherit userInfo;
          inherit configurations;
          inherit inputs;
        };
      };
    };
  };
}
