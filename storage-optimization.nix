{...}: {
  nix = {
    # gc is short for garbage collection
    #(i hate unnecessary shortenings of variable names)
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 30d";
    };
    settings.auto-optimise-store = true;
  };
}
