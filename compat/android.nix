{
  config,
  pkgs,
  userInfo,
  ...
}: {
  programs.adb.enable = true;
  environment.systemPackages = with pkgs; [
    android-tools
    waydroid
    cage # for waydroid (i use x11)
    wlroots # for cage
  ];
  users.users.${userInfo.username}.extraGroups = ["adbusers"];
}
