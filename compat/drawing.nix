{pkgs, ...}: {
  # Drivers
  hardware.opentabletdriver.enable = true;
  hardware.opentabletdriver.daemon.enable = true;
  hardware.opentabletdriver.blacklistedKernelModules = ["hid-uclogic" "wacom"];

  environment.systemPackages = with pkgs; [
    # Driver packages
    opentabletdriver

    # Drawing Programs
    krita
    inkscape
    gimp
  ];
}
