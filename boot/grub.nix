{config, ...}: {
  boot.loader = {
    #efi = {
    #  canTouchEfiVariables = true;
    #  efiSysMountPoint = "/boot/efi"; # ← use the same mount point here.
    #};
    grub = {
      enable = true;
      useOSProber = true;

      #device = "/dev/sdb";
      device = "/dev/nvme0n1";

      #efiSupport = true;
      #efiInstallAsRemovable = true; # in case canTouchEfiVariables doesn't work for your system
    };
  };
}
