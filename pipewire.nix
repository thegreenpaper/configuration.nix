{
  config,
  pkgs,
  ...
}: {
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;

  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;

    extraConfig.pipewire = {
      "10-clock-rate" = {
        "context.properties" = {
          "default.clock.rate" = 96000;
          "default.clock.allowed-rates" = [44100 48000 88200 96000 192000];
        };
      };
    };
  };

  environment.systemPackages = with pkgs; [
    pavucontrol
    helvum
  ];
}
