{
  config,
  pkgs,
  pkgs-master,
  ...
}: {
  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true; # Open ports in the firewall for Steam Remote Play
    dedicatedServer.openFirewall = true; # Open ports in the firewall for Source Dedicated Server
  };

  environment.systemPackages = with pkgs; [
    # Launchers
    heroic
    lutris
    minetest

    # Compat
    gamemode
    protonup-qt
    wine
    dxvk
    icu

    # Games
    pkgs-master.osu-lazer-bin
    xonotic

    #minecraft
    zulu8
    prismlauncher

    # modding
    r2modman

    # Depends
    (lutris.override {
      ## extra lutris depenensies
      extraPkgs = pkgs: [
        # List package dependencies here
        cabextract
        dxvk
        wineWowPackages.stable
        winetricks
      ];
    })
    mono
  ];
}
