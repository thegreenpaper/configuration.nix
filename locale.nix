{config, ...}: {
  # Set your time zone.
  time.timeZone = "Europe/Oslo";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_DK.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "nb_NO.UTF-8";
    LC_IDENTIFICATION = "nb_NO.UTF-8";
    LC_MEASUREMENT = "en_DK.UTF-8";
    LC_MONETARY = "nb_NO.UTF-8";
    LC_NAME = "en_DK.UTF-8";
    LC_NUMERIC = "nb_NO.UTF-8";
    LC_PAPER = "en_DK.UTF-8";
    LC_TELEPHONE = "nb_NO.UTF-8";
    LC_TIME = "nb_NO.UTF-8";
  };

  # Configure keymap
  console.keyMap = "no";
  services.xserver.xkb = {
    layout = "no";
    variant = "";

    # Switch capslock and escape
    options = "caps:escape";
  };
}
