# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
{
  config,
  pkgs,
  lib,
  userInfo,
  systemInfo,
  configurations,
  ...
}: let
  # Packages
  # functions
  pkgsLists = import ../pkgs/lists.nix pkgs;
in {
  imports = [
    # Include the results of the hardware scan.
    ../hardware-configuration.nix

    # server
    ./docker.nix
    ../software/server/sshd.nix
  ];

  # Bootloader.
  # This will be replaced with uefi config, automaticly generated on install when i set up the server
  # I am stupid person who uses legacy boot
  boot.loader = {
    #efi = {
    #  canTouchEfiVariables = true;
    #  efiSysMountPoint = "/boot/efi"; # ← use the same mount point here.
    #};
    grub = {
      enable = true;
      useOSProber = true;

      #device = "/dev/sdb";
      device = "/dev/nvme0n1";

      #efiSupport = true;
      #efiInstallAsRemovable = true; # in case canTouchEfiVariables doesn't work for your system
    };
  };

  networking.hostName = "brubu"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "Europe/Oslo";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  # Configure console keymap
  console.keyMap = "no";

  users = {
    # Define a user account. Don't forget to set a password with ‘passwd’.
    users.${userInfo.username} = {
      isNormalUser = true;
      description = userInfo.fullname;
      extraGroups = ["networkmanager" "wheel" "adbusers"];
      shell = pkgs.zsh;
      packages = with pkgs;
        [
        ]
        ++
        # pkgsLists (sorted by catagory then importance)
        ### Desktop
        pkgsLists "server";
    };
  };

  environment = {
    systemPackages = with pkgs;
      [
        neovim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
      ]
      ++ # pkgsLists
      pkgsLists "terminal";
  };
  fonts = {
    packages = with pkgs; [
      # Cool
      jetbrains-mono
      # Unicode and compatabillity
      noto-fonts
      noto-fonts-cjk
      noto-fonts-emoji
      font-awesome
      source-han-sans
      source-han-sans-japanese
      source-han-serif-japanese
      (nerdfonts.override {fonts = ["Meslo"];})
    ];
  };

  programs = {
    zsh = {
      enable = true;
    };
  };

  system.stateVersion = systemInfo.stateVersion; # Look at the stateVersion variable in flake.nix
}
