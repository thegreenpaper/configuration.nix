{
  config,
  lib,
  pkgs,
  userInfo,
  ...
}: let
  # In case i want variables
in {
  virtualisation.docker.enable = true;
  users.users.${userInfo.username}.extraGroups = ["docker"];

  # Changing Docker Daemon's Data Root
  virtualisation.docker.daemon.settings = {
    data-root = "/docker/data";
  };

  # Docker Containers as systemd Services
  virtualisation.oci-containers = {
    backend = "docker";
    containers = {
    };
  };
}
