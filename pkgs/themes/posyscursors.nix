# THEME INSTALL TEMPLATE
{ pkgs }:

{
pkgs.stdenv.mkDerivation {
  name = "posys-cursors";

  src = pkgs.fetchurl {
    url = "http://www.michieldb.nl/other/cursors/All%20Posy's%20Cursors%201.4.zip";
    sha256 = "sha256-8HNUAwt50hZjNDDWjn5SaMmJ2E+l0cyV9bF5I5NZ0Jw=";
  };

  dontUnpack = true;
  
  installPhase = ''
    mkdir -p $out
    ${pkgs.unzip}/bin/unzip $src -d $out/
    mv $out/Posy\'s\ Cursor\ Black $out/Posy_Black_Cursor
  '';
};
}
