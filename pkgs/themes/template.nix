# THEME INSTALL TEMPLATE
{ pkgs }:

{
pkgs.stdenv.mkDerivation {
  name = "name";

  src = pkgs.fetchurl {
    url = "https://url.domain";
    sha256 = pkgs.lib.fakeSha256 #"hash7jfusad8f0";
  };

  dontUnpack = true;
  
  installPhase = ''
    mkdir -p $out
    ${pkgs.unzip}/bin/unzip $src -d $out/
  ''
};
}
