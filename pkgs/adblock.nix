{
  lib,
  buildPythonPackage,
  fetchPypi,
  nose,
  pkgs,
}:
buildPythonPackage rec {
  version = "0.6.0";
  pname = "adblock";

  src = fetchPypi {
    inherit pname version;
    sha256 = pkgs.lib.fakeSha256;
  };

  buildInputs = [nose];

  meta = with lib; {
    description = "Python wrapper for Brave's adblocking library, which is written in Rust.";
    homepage = "https://pypi.python.org/pypi/adblock";
    license = licenses.mit;
  };
}
