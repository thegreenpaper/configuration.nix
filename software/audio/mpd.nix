{
  config,
  pkgs,
  userInfo,
  ...
}: let
  home = "/home/${userInfo.username}";
  musicfolder = "${home}/music";
  dbfolder = "${home}/.local/share/mpd";
in {
  services.mpd = {
    enable = true;
    user = userInfo.username;
    musicDirectory = "/home/${userInfo.username}/music";
    extraConfig = ''

      # file locations or something idk
      music_directory    "${musicfolder}"
      db_file            "${dbfolder}/database"
      playlist_directory "${dbfolder}/playlists"
      state_file         "${dbfolder}/state"
      sticker_file       "${dbfolder}/sticker.sql"

      # autoupdate on change
      auto_update "yes"
      #auto_update_depth "0"

      restore_paused "yes"

      audio_output {
        type "pipewire"
        name "My PipeWire Output"
      }
    '';
  };

  systemd.services.mpd.environment = {
    # https://gitlab.freedesktop.org/pipewire/pipewire/-/issues/609
    XDG_RUNTIME_DIR = "/run/user/1000"; # User-id 1000 must match above user. MPD will look inside this directory for the PipeWire socket.
    # if your userid is not 1000 then you are lost
  };

  environment.systemPackages = with pkgs; [
    ncmpcpp
    #mpdevil
  ];
}
