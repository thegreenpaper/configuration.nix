{
  config,
  pkgs,
  userInfo,
  ...
}: {
  # Enable incoming ssh
  services.openssh = {
    enable = true;
    openFirewall = true;
    settings = {
      PasswordAuthentication = false;
      PermitRootLogin = "no";
    };
  };
  users.users.${userInfo.username}.openssh.authorizedKeys.keys = [
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCbXPwCawWCulE6VpGCPYzBTXNw0or/X7l9w7jduxmnsBSJtNClTYoaYj2UBIe1w3GY32YPdV/s8t6feGw54v7iCEr2kS/OEd0MhzZ0uqnKDQ2+gKuJ394L86Ute7a6qoxL/KpWcPn5PFfvuTNI1rPWK5Uj94X/VUIzMwDbiZL5jgJjZulIAmLPvO2m7PthlgFKoGYtzl3JRBz7MR8fuALbkOmovxmHlNIjGE/5N8n4nqd5yi/j+RElxOiBVH+zg5zItnY6fJK24yyzyFbXczyFpeged8qP1ZrtB0Xl21DwB3CeBMhyPnpWMoYvbwOTO/+Fue6Roc/rkJAHnZe+tzBMXb66KHLf9rBi+zFDm4aVnwu4gIZLwm+ijtJgOwsT6aKV/GfsK6MafTE5P4qKbt5WUmlB2uOBXs2Btj9S7sfsT9cSxwAddc82SzAhQKvt4WCLOSE3YZjwWoGw1eglAZwvVjpGfUEd99Xlj4gFttid1aYFGa0jTkgsL+KuteGRlCk= jorgen@brubu"
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJuH8gFrBLVFfnerMtZIcsRRS0wmqHOYg53UFZ6KMQ6f jorgen@bruwu"
  ];
}
