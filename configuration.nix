{
  config,
  pkgs,
  lib,
  userInfo,
  systemInfo,
  ...
}: {
  # Enable Bluetooth
  hardware.bluetooth.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;
  users = {
    # Define a user account. Don't forget to set a password with ‘passwd’.
    users.${userInfo.username} = {
      isNormalUser = true;
      description = userInfo.fullname;
      extraGroups = ["wheel"];
    };
  };

  nixpkgs.config.allowUnfree = true;

 #nixpkgs.overlays = [
 #  (final: prev: {
 #    qutebrowser = prev.qutebrowser.override {
 #      enableWideVine = true; # web-drm )`:
 #      enableAdBlock = true; # idk if this works, but i hope it does
 #    };
 #  })
 #];

  fonts = {
    packages = with pkgs; [
      # Cool
      jetbrains-mono
      # Unicode and compatabillity
      noto-fonts
      noto-fonts-cjk-sans
      noto-fonts-emoji
      font-awesome
      source-han-sans
      source-han-sans-japanese
      source-han-serif-japanese
      nerd-fonts.meslo-lg
    ];
  };

  system.stateVersion = systemInfo.stateVersion; # Look at the stateVersion variable in flake.nix
}
