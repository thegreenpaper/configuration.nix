{
  config,
  lib,
  pkgs,
  stylix,
  inputs,
  configurations,
  ...
}: let
  themesDir = "themes";
  themeDir = "${themesDir}/${configurations.theme}";
  themeConf = ../${themeDir}/${configurations.theme}.yaml;
  themePolarity = lib.removeSuffix "\n" (
    builtins.readFile ../${themeDir}/polarity.txt
  );
  backgroundUrl = builtins.readFile ../${themeDir}/backgroundurl.txt;
  backgroundSha256 = builtins.readFile ../${themeDir}/backgroundsha256.txt;

  myLightDMTheme =
    if themePolarity == "light"
    then "Adwaita"
    else "Adwaita-dark";
in {
  stylix = {
    autoEnable = false;
    polarity = themePolarity;
    image = pkgs.fetchurl {
      url = backgroundUrl;
      sha256 = backgroundSha256;
    };
    base16Scheme = themeConf;
    fonts = {
      monospace = {
        name = configurations.font.name;
        package = configurations.font.pkg;
      };
      serif = {
        name = configurations.font.name;
        package = configurations.font.pkg;
      };
      sansSerif = {
        name = configurations.font.name;
        package = configurations.font.pkg;
      };
      emoji = {
        name = "Noto Color Emoji";
        package = pkgs.noto-fonts-emoji-blob-bin;
      };
    };
    targets = {
      gtk.enable = true;
    };
  };
}
