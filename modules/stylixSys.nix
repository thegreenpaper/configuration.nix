{
  config,
  lib,
  pkgs,
  stylix,
  inputs,
  configurations,
  ...
}: let
  themesDir = "themes";
  themeDir = "${themesDir}/${configurations.theme}";
  themeConf = ../${themeDir}/${configurations.theme}.yaml;
  themePolarity = lib.removeSuffix "\n" (
    builtins.readFile ../${themeDir}/polarity.txt
  );
  backgroundUrl = builtins.readFile ../${themeDir}/backgroundurl.txt;
  backgroundSha256 = builtins.readFile ../${themeDir}/backgroundsha256.txt;

  myLightDMTheme =
    if themePolarity == "light"
    then "Adwaita"
    else "Adwaita-dark";
in {
  stylix = {
    targets = {
      lightdm.enable = true;
      console.enable = true;
    };
  };
  services.xserver.displayManager.lightdm = {
    greeters.slick.enable = true;
    greeters.slick.theme.name = myLightDMTheme;
  };
  environment.sessionVariables = {
    QT_QPA_PLATFORMTHEME = "qt5ct";
  };
}
