{
  config,
  pkgs,
  ...
}: {
  services = {
    displayManager.sddm.enable = true;
    displayManager.sddm.wayland.enable = true;

    xserver = {
      # Enable the X11 windowing system.
      enable = true;

      # Enable AwesomeWM.
      windowManager.awesome.enable = true;
    };

    picom = {
      enable = true;
    };

    libinput = {
      enable = true;

      # disabling mouse acceleration
      mouse = {
        accelProfile = "flat";
        middleEmulation = false;
      };

      # disabling touchpad acceleration
      touchpad = {
        accelProfile = "flat";
      };
    };
  };

  programs = {
    dconf = {
      enable = true;
    };
    nm-applet = {
      enable = true;
    };
  };

  environment.systemPackages = with pkgs; [
    # tools
    arandr
    xorg.xev
    xorg.xkbcomp

    # wm depends
    flameshot
    rofi
    tdrop
    xfce.xfce4-power-manager
    xclip

    # work
    firefox
    obsidian
    pandoc
    libreoffice
    mpv

    # school
    python3
    geogebra6
    lua

    # communication
    discord
    signal-desktop
    element-desktop
    thunderbird
    teams-for-linux # :(

    # dev (probably gonna move this later)
    jetbrains.idea-ultimate
    mariadb
    sqlite
    lua
    ## laravel ##
    php
    php82Packages.composer
    nodejs_18

    # media production
    #blender
    gimp

    # files
    nemo
    nemo-fileroller
    file-roller
    qbittorrent
  ];
}
