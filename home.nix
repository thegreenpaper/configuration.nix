{
  # nixConf
  config,
  pkgs,
  lib,
  # installInfo
  systemInfo,
  userInfo,
  # Theme
  inputs,
  configurations,
  ...
}: {
  imports = [
    # style
    inputs.stylix.homeManagerModules.stylix
    ./modules/stylix.nix
  ];
  home = {
    /*
    The home.stateVersion option does not have a default and must be set
    */
    stateVersion = systemInfo.stateVersion;
    username = userInfo.username;
    homeDirectory = "/home/" + userInfo.username;
  };

  home.file = {
  };

  home.pointerCursor = {
    package = pkgs.dracula-theme;
    name = "Dracula-cursors";
  };
  gtk.iconTheme = {
    package = pkgs.dracula-icon-theme;
    name = "Dracula-Icons";
  };
  programs = {
    home-manager.enable = true;
    direnv = {
      enable = true;
      enableZshIntegration = true;
    };
    zsh = {
      enable = true;
      autosuggestion.enable = true;
      syntaxHighlighting.enable = true;
      enableCompletion = true;
      history = {
        size = 10000;
        save = 10000;
        # Never mind didn't work
        #path = "$XDG_CACHE_HOME/zsh/history";
        path = "/home/${userInfo.username}/.cache/zsh/history";
      };
      shellAliases = {
        ## nixos
        rebuildbrubu = "sudo nixos-rebuild switch --flake ~/.nixos#system && home-manager switch --flake path:~/.nixos#user";
        rebuildbruwu = "sudo nixos-rebuild switch --flake path:~/.nixos#worksys && nix run home-manager/master -- switch --flake path:~/.nixos#user";

        ## Simple alias
        v = "nvim";
        neofetch = "fastfetch";
        nv = "vim";
        ls = "ls -a";
        la = "ls -lah";
        #l. = "ls -a | egrep \"^\.\"";
        grep = "grep --color=auto";
        egrep = "egrep --color=auto";
        fgrep = "fgrep --color=auto";
        du = "du -h";
        df = "df -h";
        lynx = "lynx -vikeys searx.lukesmith.xyz";
        cp = "cp -i";
        ssn = "sudo shutdown now";
        sr = "sudo reboot";
        rr = "curl -s -L https://raw.githubusercontent.com/keroserene/rickrollrc/master/roll.sh | bash";
        gnome-control-center = "XDG_CURRENT_DESKTOP=GNOME gnome-control-center";
        dmenu = "dmenu -nb #111c26 -fn '#111c26'";
        #pip = "pipsi";
        lsblk = "lsblk | grep -v 'loop'";
        aliasrc = "vim /home/$USER/.config/aliasrc";

        # Navigation
        server = "cd /home/${userInfo.username}/hobby/serverhost/minecraft";
        nixrc = "cd /etc/nixos";
        ".." = "cd ..";
        "..." = "cd ../..";
        ".3" = "../../..";
        ".4" = "../../../..";
        ".5" = "../../../../..";

        # Sudo
        sudo = "sudo --preserve-env=HOME";
      };
      initExtra = ''
        PS1="%B%{$fg[yellow]%}[%{$fg[red]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[yellow]%}]%{$reset_color%}$%b "
        fastfetch
      '';
    };
    git = {
      enable = true;
      userName = userInfo.username;
      userEmail = userInfo.email;
      extraConfig.safe.directory = "etc/nixos";
    };
    neovim = {
      enable = true;
      defaultEditor = true;
      viAlias = true;
      vimAlias = true;
      extraConfig = ''
        set expandtab
        set tabstop=2
        set softtabstop=2
        set shiftwidth=2
        set number
        set relativenumber
        set mouse=a
        set clipboard+=unnamedplus
      '';
      #extraLuaConfig = ''
      #  -- is this how objects work in lua?
      #  vim.opt = {
      #    number = true,
      #    relativenumber = true,
      #    mouse = "a",
      #    clipboard = "unnamedplus",
      #  }
      #'';
    };
   #qutebrowser = {
   #  enable = true;
   #  searchEngines = {
   #    b = "https://search.brave.com/search?q={}";
   #    wb = "http://wiby.me/?q={}";
   #    ns = "https://search.nixos.org/packages?query={}";
   #    nw = "https://nixos.wiki/index.php?search={}";
   #  };
   #};
  };
  xdg = {
    enable = true;
    userDirs = {
      extraConfig = {
        XDG_GAME_DIR = "${config.home.homeDirectory}/Media/Games";
        XDG_GAME_SAVE_DIR = "${config.home.homeDirectory}/Media/Game Saves";
      };
    };
    configFile = {
      "alacritty" = {
        source = ./pkgs/config/alacritty;
        recursive = true;
      };
      "awesome" = {
        source = ./pkgs/config/awesome;
        recursive = true;
      };
    };
  };
}
